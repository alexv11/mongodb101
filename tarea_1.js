// Pregunta 2
// El conjunto de datos contiene 4 calificaciones de n estudiantes.
// Confirma que se importó correctamente la colección con los siguientes comandos
// en la terminal de mongo
// use students; db.grades.count()
// ¿Cuántos registros arrojó el comando count?
// Respuesta: 800

// Pregunta 3
// Encuentra todas las calificaciones del estudiante con el id número 4
// db.grades.find({student_id: 4})
// Respuesta: [ { "_id" : ObjectId("50906d7fa3c412bb040eb589"), "student_id" : 4, "type" : "exam", "score" : 87.89071881934647 }, { "_id" : ObjectId("50906d7fa3c412bb040eb588"), "student_id" : 4, "type" : "quiz", "score" : 27.29006335059361 }, { "_id" : ObjectId("50906d7fa3c412bb040eb587"), "student_id" : 4, "type" : "homework", "score" : 5.244452510818443 }, { "_id" : ObjectId("50906d7fa3c412bb040eb586"), "student_id" : 4, "type" : "homework", "score" : 28.656451042441 } ]

// Pregunta 4
// ¿Cuántos registros hay de tipo exam?
// db.grades.find({type: "exam"}).count()
// Respuesta: 200

// Pregunta 5
// ¿Cuántos registros hay de tipo homework?
// db.grades.find({type: "homework"}).count()
// Respuesta: 400

// Pregunta 6
// ¿Cuántos registros hay de tipo quiz?
// db.grades.find({type: "quiz"}).count()
// Respuesta: 200

// Pregunta 7
// Elimina todas las calificaciones del estudiante con el id número 3
// db.grades.remove({student_id: 3})
// Respuesta: WriteResult({ "nRemoved" : 4 })

// Pregunta 8
// ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea?
// db.grades.find({score: 75.29561445722392})
// Respuesta: [ { "_id" : ObjectId("50906d7fa3c412bb040eb5a0"), "student_id" : 10, "type" : "homework", "score" : 75.29561445722392 }, { "_id" : ObjectId("50906d7fa3c412bb040eb5a1"), "student_id" : 6, "type" : "homework", "score" : 75.29561445722392 } ]

// Pregunta 9
// Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100
// db.grades.update({_id: ObjectId("50906d7fa3c412bb040eb591")}, {$set: {score: 100}})
// Respuesta: WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })

// Pregunta 10
// ¿A qué estudiante pertenece esta calificación?
// db.grades.find({_id: ObjectId("50906d7fa3c412bb040eb591")})
// Respuesta: [ { "_id" : ObjectId("50906d7fa3c412bb040eb591"), "student_id" : 7, "type" : "quiz", "score" : 100 } ]
